# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0 filetype=yaml:
#
# This CI uses the freedesktop.org ci-templates.
# Please see the ci-templates documentation for details:
# https://freedesktop.pages.freedesktop.org/ci-templates/

.templates_sha: &template_sha 34f4ade99434043f88e164933f570301fd18b125 # see https://docs.gitlab.com/ee/ci/yaml/#includefile


include:
  # Arch container builder template
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file: '/templates/arch.yml'
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file: '/templates/ci-fairy.yml'
  - template: Security/SAST.gitlab-ci.yml


stages:
  - prep             # prep work like rebuilding the container images if there is a change
  - build            # for actually building and testing things in a container
  - test
  - deploy


variables:
  FDO_UPSTREAM_REPO: 'xorg/util/cf'
  # The tag should be updated each time the list of packages is updated.
  # Changing a tag forces the associated image to be rebuilt.
  # Note: the tag has no meaning, we use a date format purely for readability
  FDO_DISTRIBUTION_TAG:  '2023-02-15.0'
  # minimal set of packages required to build and install either way
  BASE_PACKAGES:  'git gcc pkgconf xorg-font-util'
  # packages needed to build and install with each set of tools
  AUTOTOOLS_PACKAGES: 'autoconf automake make xorg-util-macros'
  MESON_PACKAGES: 'meson ninja'
  # extra packages we need for various tests
  EXTRA_PACKAGES: 'diffutils jq'
  FDO_DISTRIBUTION_PACKAGES: $BASE_PACKAGES $AUTOTOOLS_PACKAGES $MESON_PACKAGES $EXTRA_PACKAGES

#
# Verify that commit messages are as expected
#
check-commits:
  extends:
    - .fdo.ci-fairy
  stage: prep
  script:
    - ci-fairy check-commits --junit-xml=results.xml
  except:
    - master@xorg/util/cf
  variables:
    GIT_DEPTH: 100
  artifacts:
    reports:
      junit: results.xml

#
# Verify that the merge request has the allow-collaboration checkbox ticked
#
check-merge-request:
  extends:
    - .fdo.ci-fairy
  stage: deploy
  script:
    - ci-fairy check-merge-request --require-allow-collaboration --junit-xml=results.xml
  artifacts:
    when: on_failure
    reports:
      junit: results.xml
  allow_failure: true


#
# Build a container with the given tag and the packages pre-installed.
# This only happens if the tag changes, otherwise the existing image is
# re-used.
#
container-prep:
  extends:
    - .fdo.container-build@arch
  stage: prep
  variables:
    GIT_STRATEGY: none


#
# The default build, runs on the image built above.
#
autotools:
  stage: build
  extends:
    - .fdo.distribution-image@arch
  script:
    - mkdir _builddir
    - pushd _builddir > /dev/null
    - ../autogen.sh --disable-silent-rules
    - make
    - make check
    - make distcheck
    - mv xorg-cf-files-*.tar.gz ..
    - popd > /dev/null
  artifacts:
    paths:
      - xorg-cf-files-*.tar.gz

meson:
  extends:
    - .fdo.distribution-image@arch
  stage: build
  script:
    - mkdir -p ../_inst
    - meson setup builddir --prefix="$PWD/../_inst" $MESON_OPTIONS
    - meson compile -C builddir
    - meson install -C builddir

meson from tarball:
  extends:
    - .fdo.distribution-image@arch
  stage: test
  script:
    - mkdir -p _tarball_build
    - tar xf xorg-cf-files-*.tar.gz -C _tarball_build
    - pushd _tarball_build/xorg-cf-files-*
    - meson setup builddir
    - meson test -C builddir
  needs:
    - autotools
  variables:
    GIT_STRATEGY: none

compare meson and autotools:
  extends:
    - .fdo.distribution-image@arch
  stage: test
  script:
    - mkdir -p $PWD/_meson_inst
    - mkdir -p $PWD/_autotools_inst
    # the prefix ends up in the site.def file, so we use a symlink
    # to use the same --prefix for meson and autotools
    - ln -sf $PWD/_meson_inst $PWD/_inst
    - meson setup builddir --prefix=$PWD/_inst
    - meson compile -C builddir
    - meson install -C builddir
    - rm $PWD/_inst
    - ln -sf $PWD/_autotools_inst $PWD/_inst
    - ./autogen.sh --prefix=$PWD/_inst
    - make && make install
    - diff --brief --recursive $PWD/_meson_inst $PWD/_autotools_inst

check versions are in sync:
  extends:
    - .fdo.distribution-image@arch
  stage: test
  script:
    - autoreconf -ivf
    - ./configure --version | head -n 1 | sed -e 's/xorg-cf-files configure //' > autotools.version
    - meson setup builddir
    - meson introspect --projectinfo builddir | jq -r '.version' > meson.version
    - diff -u autotools.version meson.version || (echo "ERROR - autotools and meson versions not in sync" && false)
